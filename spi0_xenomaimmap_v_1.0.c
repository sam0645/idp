#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//Xenomai:
#include <native/task.h>
#include <native/timer.h>
#include <signal.h>
#include <unistd.h>
#include <sys/mman.h>

#include <linux/spi/spidev.h>
#include "hw_mcspi.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define TASK_PRIO 99 // Highest RT priority 0-99 //
#define TASK_MODE 0 // No flags //
#define TASK_STKSZ 0 // Stack size (use default one)//
//#define _GNU_SOURCE

#define CM_PER_BASE     0x44E00000  /* base address of clock control regs */
#define CM_PER_SPI0_CLKCTRL     0x4C        /* offset of SPI0 clock control reg */
#define SPIO_CLKCTRL_MODE_ENABLE 0x2          /* value to enable SPI0 clock */

#define CONTROL_MODULE_BASE 0x44E10000 /*base address for the control module*/
#define CONTROL_MODULE_END  0x44E11FFF // size is 8KB
#define CONF_SPI0_SCLK 0x950
#define CONF_SPI0_DO 0x954
#define CONF_SPI0_D1 0x958
#define CONF_SPI0_CS0 0x95C
#define CONF_SPI0_CS1 0x960



//Address defines://
#define MCSPI0_START_ADDR 0x48030000  // (start of 4KB reserved)
#define MCSPI0_END_ADDR 0x48030FFF  //(end of 4KB reserved)
#define MCSPI0_SIZE (MCSPI0_END_ADDR - MCSPI0_START_ADDR)

#define MCSPI1_START_ADDR 0x481A0000  // (start of 4KB reserved)
#define MCSPI1_END_ADDR 0x481A0FFF  //(end of 4KB reserved)
#define MCSPI1_SIZE (MCSPI1_END_ADDR - MCSPI1_START_ADDR)

int main() {

	int fd;
	struct stat attr;

	uint8_t tx[] = {
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
		0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B,
		0x0C, 0x0D, 0x0E, 0x0F, 0x00, 0x01,
		0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D,
		0x0E, 0x0F, 0x01, 0x02, 0x03, 0x04,
		0x05, 0x06, 0x07
	};
	uint8_t rx[ARRAY_SIZE(tx)] = {0, };

	// Disable paging for this program's memory.
	mlockall(MCL_CURRENT | MCL_FUTURE);

	// Enable rt_print
	rt_print_auto_init(1);

	//Register addresses://
	void *mcspi0_addr = NULL, *pClockControl = NULL, *pL4lsStClkControl = NULL, *pL4lsClkControl = NULL;
	volatile unsigned int * addr_MCSPI_REVISION = NULL, 
				*addr_MCSPI_SYSCONFIG = NULL,
				*addr_MCSPI_SYSSTATUS = NULL,
				*addr_MCSPI_IRQSTATUS = NULL,
				*addr_MCSPI_IRQENABLE = NULL,
				*addr_MCSPI_SYST = NULL,
				*addr_MCSPI_MODULCTRL = NULL,
				*addr_MCSPI_CH0CONF = NULL,
				*addr_MCSPI_CH0STAT = NULL,
				*addr_MCSPI_CH0CTRL = NULL,
				*addr_MCSPI_TX0 = NULL,
				*addr_MCSPI_RX0 = NULL,
				*addr_MCSPI_CH1CONF = NULL,
				*addr_MCSPI_CH1STAT = NULL,
				*addr_MCSPI_CH1CTRL = NULL,
				*addr_MCSPI_TX1 = NULL,
				*addr_MCSPI_RX1 = NULL,
				*addr_MCSPI_CH2CONF = NULL,
				*addr_MCSPI_CH2STAT = NULL,
				*addr_MCSPI_CH2CTRL = NULL,
				*addr_MCSPI_TX2 = NULL,
				*addr_MCSPI_RX2 = NULL,
				*addr_MCSPI_CH3CONF = NULL,
				*addr_MCSPI_CH3STAT = NULL,
				*addr_MCSPI_CH3CTRL = NULL,
				*addr_MCSPI_TX3 = NULL,
				*addr_MCSPI_RX3 = NULL,
				*addr_MCSPI_XFERLEVEL = NULL,
				*addr_MCSPI_DAFTX = NULL,
				*addr_MCSPI_DAFRX = NULL;

	//Memory mapping
	if ((fd = open("/dev/mem", O_RDWR | O_SYNC)) < 0) {
		fprintf(stderr, "Couldn't open /dev/mem!\n");
		exit(fd);
	} else {
		printf("\nMapping %X - %X (size: %X)\n", MCSPI0_START_ADDR, MCSPI0_END_ADDR,
				MCSPI0_SIZE);
	}

	if (fstat(fd, &attr) == -1) {
		fprintf(stderr, "Possible file descriptor error!\n");
	}

	mcspi0_addr = mmap(0, 4096, PROT_READ | PROT_WRITE, MAP_SHARED, fd, MCSPI0_START_ADDR); 

	if (mcspi0_addr == MAP_FAILED) {
		printf("Unable to map SPI.\n");
		exit(1);
	}

	printf("mcspi0_addr: %p\n", mcspi0_addr);
	// munmap(mcspi0_addr, MCSPI0_SIZE);

	// MCSPI register offsets
	addr_MCSPI_SYSCONFIG = mcspi0_addr + MCSPI_SYSCONFIG;
	addr_MCSPI_SYSSTATUS = mcspi0_addr + MCSPI_SYSSTATUS;
	addr_MCSPI_MODULCTRL = mcspi0_addr + MCSPI_MODULCTRL;
	addr_MCSPI_IRQENABLE = mcspi0_addr + MCSPI_IRQENABLE;
	addr_MCSPI_IRQSTATUS = mcspi0_addr + MCSPI_IRQSTATUS;
	addr_MCSPI_CH0CONF   = mcspi0_addr + MCSPI_CHCONF(0);
	addr_MCSPI_CH0CTRL   = mcspi0_addr + MCSPI_CHCTRL(0);
	addr_MCSPI_TX0	     = mcspi0_addr + MCSPI_TX(0);
	addr_MCSPI_RX0	     = mcspi0_addr + MCSPI_RX(0);
	addr_MCSPI_XFERLEVEL = mcspi0_addr + MCSPI_XFERLEVEL;
	addr_MCSPI_SYST = mcspi0_addr + MCSPI_SYST;
	addr_MCSPI_CH0STAT = mcspi0_addr + MCSPI_CHSTAT(0);
	addr_MCSPI_CH1CTRL = mcspi0_addr + MCSPI_CHCTRL(1);
	addr_MCSPI_CH2CTRL = mcspi0_addr + MCSPI_CHCTRL(2);
	addr_MCSPI_CH3CTRL = mcspi0_addr + MCSPI_CHCTRL(3);
	/*================================== CLOCKS resposible for SPI module INITIALIZTION ========================================*/
	// map a pointer to the clock control block:
	pClockControl = (char *)mmap(0, 4096, PROT_READ|PROT_WRITE, MAP_SHARED, fd, CM_PER_BASE);

	if(pClockControl == (char *)0xFFFFFFFF)
	{
        	printf("Memory map failed.error %i\n", (uint32_t)pClockControl);
	        close( fd );
        	return 2;
	}

	printf("pClockControl = 0x%08X\n", (uint32_t)(pClockControl));

	uint32_t value = *(uint32_t *)(pClockControl + CM_PER_SPI0_CLKCTRL);
	printf("1. CM_PER_SPI0_CLKCTRL before resetting 0x%08X\n", value);

	*(uint32_t *)(pClockControl + CM_PER_SPI0_CLKCTRL) = SPIO_CLKCTRL_MODE_ENABLE; //0x02; value used for resetting the SPI0 clock
	value = *(uint32_t *)(pClockControl + CM_PER_SPI0_CLKCTRL);
	printf("2. CM_PER_SPI0_CLKCTRL after resetting 0x%08X\n", value);
         
	while( value != SPIO_CLKCTRL_MODE_ENABLE )
	{
        	// poll until the SPI clock is enabled
	        value = *(uint32_t *)(pClockControl + CM_PER_SPI0_CLKCTRL);
        	printf("3. CM_PER_SPI0_CLKCTRL now 0x%08X\n", value);
    	}
	
	pL4lsStClkControl = pClockControl; //address of L4ls power state transition register
	pL4lsClkControl = pClockControl + 0x60; // address of L4Ls clocks register
	
		
	if (!((*(uint32_t *)pL4lsStClkControl))) {
		printf("going to set the L4LS Power State Transition register");
		*(uint32_t *)pL4lsStClkControl = 0x02; //enabling the L4LS power state transition register 
	}
	value = *(uint32_t *)(pL4lsStClkControl);
	printf("value of L4LS Power State Transition register's is 0x%08X\n\n", value);
	if (!((*(uint32_t *)pL4lsClkControl))) {
		printf("going to set the L4LS Power State Transition register");
		*(uint32_t *)pL4lsStClkControl = 0x02; // enabling the L4LS register
	}
	value = *(uint32_t *)(pL4lsClkControl);
	printf("value of L4LS clock contrl register's is 0x%08X\n\n", value);
		
	munmap(pClockControl,4096);
	
			/*===========================Now going to reset SPI Module ========================*/
	// Reset the SPI controller
    	*addr_MCSPI_SYSCONFIG = 0x2; // value used to reset the module with the help of this register
	value = *(uint32_t *)addr_MCSPI_SYSCONFIG;
	printf("value of the MCSPI_SYSCONFIG is 0x%08X\n",value);

	// Wait until resetting is done
	unsigned int dwCount = 0;
	while ( (*addr_MCSPI_SYSSTATUS & 0x1) != 0x1) { 
		printf("SPI reset ongoing!!\n");        	
		sleep (1);
	        if (dwCount++>0x100){
        	    printf("SPI: ERROR holding in reset.\n");
	            exit(1);
        	}
    	}
			/*============== SPI Module resetting is done till here ===============*/	
	value = *(uint32_t *)(addr_MCSPI_SYSSTATUS);
	if (value == 0x1) {
		printf("SPI module reset completed!\n");
	}
	printf("\nState of the registers just after Module reset is done with the help of register MCSPI_SYSCONFIG==================>\n\n");
	value = *(uint32_t *)addr_MCSPI_SYSCONFIG;
	printf("value of the MCSPI_SYSCONFIG is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_SYSSTATUS;
	printf("value of the MCSPI_SYSSTATUS is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_IRQSTATUS;
	printf("value of the MCSPI_IRQSTATUS is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_IRQENABLE;
	printf("value of the MCSPI_IRQENABLE is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_SYST;
	printf("value of the MCSPI_SYST is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_MODULCTRL;
	printf("value of the MCSPI_MODULCTRL is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_CH0CONF;
	printf("value of the MCSPI_CH0CONF is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_CH0STAT;
	printf("value of the MCSPI_CH0STAT is 0x%08X\n",value);
	value = *(uint32_t *)addr_MCSPI_CH0CTRL;
	printf("value of the MCSPI_CH0CTRL is 0x%08X\n",value);
	printf("==============================================================================================\n\n");

	//cofiguring the serial port interface
	*addr_MCSPI_MODULCTRL = 0x1; 
	/*
	 0x01 means that FiFo disabled,
	 Multiple word access disabled,
	 initial delay for transferring first word is disabled,
	 module is brought is functional mode,
	 acting as a Master,
         SPIEN is enabled,
	 only one channel is used in Master mode
	*/

	value = *(uint32_t *)addr_MCSPI_MODULCTRL;
	printf("value of the MCSPI_MODULCTRL is 0x%08X\n",value);
	
	//enabling the Interrupts 
	*addr_MCSPI_IRQENABLE = 0x5; // enabling interrupts TX0_Empty and RX0_Full
	

	//configuring the channel 0
	*addr_MCSPI_CH0CONF = 0x103C9;//0x60394
	/*
	the value above i.e. 0x000103C9 means the following
	0 <- means clock granularity is of the form power of 2, FIFO for read disabled 
	0 <- FIFO for write disabled, 0.5 clock cycles between CS toggling, Start bit polartiy is 0
	0 <- Start bit disabled,SPIEN enabled on channel 0, FORCE bit is disabled is very important;
	1 <- Turbo mode disabled, Dataline 0 for reception, Dataline 1 for transmission
	0 <- DMA read disabled, DMA write disabled, Transmit and recieve mode enabled
	3C <- Wordlength is 8 bits(1 byte), SPIEN is active low, frequency divider for SPICLK is 2^2
	9 <- SPICLK polarity is active high, SPICLK phase as the data is latched at odd numbered edges of SPICLK
	*/	


	
	value = *(uint32_t *)addr_MCSPI_CH0CONF;
	printf("value of the MCSPI_CH0CONF now is 0x%08X\n",value); 

	// start SPI CH0.....Enabling channel 0 and JUST TO BE SURE Disabling channel 1,2,3
	*addr_MCSPI_CH0CTRL = 0x1;//value used to enable the channel 0
	value = *(uint32_t *)addr_MCSPI_CH0CTRL;
	printf("value of the MCSPI_CH0CTRL now is 0x%08X\n",value); 


	/* 
	 Data transfer with the help of 
	 first data written to the Tx register
	 and then data is read from the Rx register
	*/
	uint8_t count;
	for (count = 0; count < ARRAY_SIZE(tx); count++) 
	{		
		*addr_MCSPI_CH0CONF |= 0x00100000; // enabling spien by setting FORCE bit =1
		//value = *(uint32_t *)addr_MCSPI_IRQSTATUS;

		/*
		 Polling if the TX0_Empty event to occured,
		 after which data is put on the TX0 register
		*/
		while((*addr_MCSPI_IRQSTATUS & 0x1) != 0x1) {  		
			sleep(1);	
			value = *(uint32_t *)addr_MCSPI_CH0STAT;
			printf("\nvalue of MCSPI_CH0STAT is 0x%08X\n", value);
		}
		value = *(uint32_t *)addr_MCSPI_IRQSTATUS;		
		printf("\nfound TX_Empty event 0x%08X\n", value);		
		*addr_MCSPI_TX0 = tx[count];
		
		/*
		 Polling if the RX0_Full event has occured,
		 after which data is read from the RX0 register
		*/
		while((*addr_MCSPI_IRQSTATUS & 0x4) != 0x4) {
			sleep(1);			
		}
		value = *(uint32_t *)addr_MCSPI_IRQSTATUS;
		printf("\nfound RX_full event 0x%08X\n", value);
		rx[count] = *(uint8_t*)addr_MCSPI_RX0;
		*addr_MCSPI_CH0CONF &= ~(0x00100000); // disabling spien by setting FORCE bit =0
	
		/* 
		Note: this enabling/disabling of the SPIEN between
		the word transfer is done as described in the
		manual with the help of FORCE bit
		*/
	} 
		
	
	for (count = 0; count < ARRAY_SIZE(tx); count++)
        {
		printf("%08X \n", rx[count]);
        }
	printf("\n");

	// disabling the Channel 0 
        *addr_MCSPI_CH0CTRL &= (uint32_t)0x0; // value used to disable the channel 0
	value = *(uint32_t *)addr_MCSPI_CH0CTRL;
	printf("value of the MCSPI_CH0CTRL after disabling is 0x%08X\n",value); 
	munmap(mcspi0_addr, MCSPI0_SIZE + 0x01);
	close(fd);
}



